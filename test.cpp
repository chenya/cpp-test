#include <iostream>
using namespace std;

unsigned int fib(unsigned int n);

int main(void)
{	
	unsigned int num;

	cout << "Enter NUM: ";
	cin >> num;
	cout << endl;
	cout << "fib(" << num << ") = " << fib(num) << endl;
	return 0;
}

unsigned int fib(unsigned int n)
{
	return (n<3) ? 1 : fib(n-1) + fib(n-2);

}
